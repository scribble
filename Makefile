
install:
	cp scribble.py /usr/bin
	chmod a+rx /usr/bin/scribble.py
	cp scribble.desktop /usr/share/applications
	chmod a+r /usr/share/applications/scribble.desktop
	cp scribble.png /usr/share/pixmaps/scribble.png
	chmod a+r /usr/share/pixmaps/scribble.png
	if [ -d $$HOME/Pages ] ; then : ; else cp -r Sample-Pages $$HOME/Pages; fi
